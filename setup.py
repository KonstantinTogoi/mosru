"""Setup."""
from os.path import dirname, join

from setuptools import find_packages, setup

readme_path = join(dirname(__file__), 'README.md')


with open(readme_path) as readme_file:
    readme = readme_file.read()


setup(
    name='mosru',
    version='1.0.0',
    author='Konstantin Togoi',
    author_email='konstantin.togoi@gmail.com',
    url='https://gitlab.com/KonstantinTogoi/mosru',
    description='Test API of mos.ru.',
    long_description=readme,
    long_description_content_type='text/markdown',
    license='BSD',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'aiohttp==3.8.1',
        'click==8.1.3',
        'elasticsearch==8.3.1',
        'pyyaml==6.0',
    ],
    setup_requires=[
        'pytest-runner==6.0.0',
    ],
    tests_require=[
        'pytest==7.1.2',
        'pytest-aiohttp==1.0.4',
        'pytest-asyncio==0.18.3',
    ],
    keywords=[
        'elasticsearch',
        'mos.ru',
    ],
    classifiers=[
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP',
    ],
    entry_points={
        'console_scripts': [
            'mosru=mosru.__main__:main',
        ],
    },
)
