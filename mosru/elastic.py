"""Функции для работы с Elasticsearch."""
import csv
import hashlib
from typing import Dict

from elasticsearch import Elasticsearch
from loguru import logger as log

mappings = {
    'mappings': {
        'properties': {
            'doc': {'type': 'text'},
            'query': {'type': 'percolator'},
        },
    },
}


def populate(conn: Dict[str, str], queries: str):  # noqa: WPS210
    """Populates percolator in Elasticsearch with queries.

    Args:
        conn (Dict[str, str]): connection to Elasticsearch
        queries (str): path to a .csv file with format

            "topic1","query1"
            "topic1","query2"
            "topic2","query1"

    Returns:
        Elasticsearch

    """
    es: Elasticsearch = Elasticsearch(**conn)

    # create indices
    with open(queries) as cf:
        reader = csv.reader(cf, delimiter=',', quotechar='"')

        for num, (topic, query) in enumerate(reader):
            if not es.indices.exists(index=topic):
                es.indices.create(index=topic, body=mappings, ignore=400)

            terms = (term.lower() for term in query.split())
            body = {'query': {'bool': {'filter': [
                {'term': {'doc': term}} for term in terms
            ]}}}

            # hash id for duplicate prevention
            qhash = hashlib.md5(query.encode('utf-8'))  # noqa: S324
            es.index(
                id=qhash.hexdigest(),
                index=topic,
                body=body,
                ignore=400,
            )

        es.indices.refresh()
        log.info(f'Successfully indexed {num + 1} queries.')

    return es
