"""Conftest."""
import asyncio
import os
from os.path import dirname, join
from typing import List, Tuple

import pytest
from aiohttp.web import Application

from mosru import mosapp, elastic

here = dirname(__file__)


@pytest.fixture(scope='session')
def event_loop():
    """Event loop."""
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def app() -> Application:
    """Application."""
    url = os.environ.get('ES_URL', 'http://localhost:9200')
    conn = {'hosts': url}
    es = elastic.populate(conn, join(here, 'fixtures', 'queries.csv'))
    mosapp['elasticsearch'] = es
    return mosapp


@pytest.fixture(params=[
    ('где купить зимние шины', ['товары']),
    ('борща любимого рецепт', ['кухня']),
    ('тайская кухня', ['кухня', 'товары']),
    ('кухня', []),
])
def query_topics(request: pytest.FixtureRequest) -> Tuple[str, List[str]]:
    """Reference query and topics."""  # noqa: D401
    return request.param[0], request.param[1]  # type: ignore
