FROM python:3.9-slim as base

# install project
WORKDIR /usr/src/app
COPY . .

# install waitc
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait
